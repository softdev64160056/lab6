package com.mycompany.lab6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author ACER
 */
public class TestWriteFriend {

    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend f1 = new Friend("Pasinee", 21, "086664512");
            Friend f2 = new Friend("Ging", 20, "089664512");
            System.out.println(f1);
            System.out.println(f2);
            File file = new File("friend.det");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(f1);
            oos.writeObject(f2);
            oos.close();
            fos.close();
        } catch (Exception ex) {
            System.out.println("Write Error!!!");
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }

            } catch (IOException ex) {
                Logger.getLogger(TestWriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
